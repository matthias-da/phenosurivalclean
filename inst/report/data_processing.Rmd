---
title: "phenoSurvival Report"
author: "Stefan Fleck"
date: '2015-03-25'
output: 
  html_document:
    toc: true
---

```{r, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_knit$set(root.dir = '../')
 setwd('/home/hoelk/Dropbox/workspace/r/phenosurivalclean/')
```

```{r, echo=FALSE, warning=FALSE, message=FALSE}
# install.packages("ncdf", type = "source", configure.args="--with-netcdf-include=/usr/include")
library(devtools)
library(rgdal)
library(rgeos)
library(raster)
load_all("./")
```

**Needs some cleanup/reworking to be up-to date with new package version**

This document shows how the data for plots.Rmd and hazard_model.Rmd was processed

* External datasets (R binary format):
    * aggNao.rda
    * aggNaom.rda
    * fenodata.rda
    * fenost.rda
    * nao.rda
    * pheno.rda


* External datasets (Other formats):
    * EOBS climate data: tg_0.25deg_reg_v10.0.nc, tn_0.25deg_reg_v10.0.nc, tx_0.25deg_reg_v10.0.nc, rr_0.25deg_reg_v10.0.nc
    * EOBS elevation data (not used): elev_0.25deg_reg_v10.0.nc
    * Finnish phenological data: StationsFinn.csv, TaraxacumBF.csv, ConvallariaBF.csv
  

* Processed datasets created by this document (R binary format):
    * fenost_biogeo: Phenological stations with biogeoregion information
    * fenodata_fin: Phenological data for Finnland
    * fenodata_all: fenodata.rda & fenodata_fin.rda
    * td_covar: Time depnend covariates (EOBS climate data)
    * td_covar_monthly: Monthly aggregate statistics of the EOBS climate data
    * feno_map: Data used for creating ggmap plots of the fenostations


# Notes / Todos

* Documents how the datasets in the data directory were created, does not need to be run
* Check if we loose data when discarding duplicate fenostations (see process stations chapter)
* Why does pheno have less stations rows than fenodata/fenost?
* pheno is missing some stations in hungary and one in sk for no apparent reason

```{r}
MAX_MONTH = 8 # Set max month for phenological and climate data
```


```{r}
data(fenodata)
data(fenost)
data(pheno)

length(unique(fenost$stid))
length(unique(fenodata$stid))
length(unique(pheno$stid))
```


# Prepare phenological datasets

## Add Biogeoregions to phenostations

```{r}
data(fenost)
biogeo = readOGR('/home/hoelk/Datasets/pheonosurvival/biogeo/', 'BiogeoRegions2015')
fenost_biogeo = find_biogeoregions(fenost, biogeo)

# Manually fix some stations that not get assigned the appropriate biogeoregions
fenost_biogeo[is.na(fenost_biogeo$biogeo), 'stid'] %>% 
  as.character() %>% 
  sort()

fenost_biogeo[grepl('pfi', fenost_biogeo$stid),]$biogeo = 'Boreal'
fenost_biogeo[fenost_biogeo$stid == 'phr008',]$biogeo = 'Mediterranean'

save(fenost_biogeo, file = './data/fenost_biogeo.rda')
```


## Process additional phenostations and phenodata for Finnland

```{r, highlight=TRUE}
data(fenodata)
data(fenost_biogeo)

data_dir = '/home/hoelk/Datasets/pheonosurvival/fin_csv/'


# Read Finnland data from csv  ---------------------------

  conmaj<- read.table(paste(data_dir, "ConvallariaBF.csv", sep=""), header=TRUE, sep=";") %>% 
    data.table(key = c('YHTKOLEV', 'YHTKOPIT'))
  tarax<- read.table(paste(data_dir, "TaraxacumBF.csv", sep=""), header=TRUE, sep=";") %>% 
    data.table(key = c('YHTKOLEV', 'YHTKOPIT'))
  stations<- read.table(paste(data_dir, "StationsFinn.csv", sep=""), header=TRUE, sep=";") %>% 
    data.table(key = c('YHTKOLEV', 'YHTKOPIT'))


# Convert lattidue/longitue to numeric:  ---------------------------
  
  stations$coords.x1 = gsub(',', '.', stations$coords.x1) %>% 
    as.numeric() 
  stations$coords.x2 = gsub(',', '.', stations$coords.x2) %>% 
    as.numeric()


# Find closest eobs station  ---------------------------
  
  stations = dplyr::rename(stations, lat = coords.x2, lon = coords.x1, lat_deg = lat, lon_deg = lon)
  stations_grid = find_closest_gridpoint(stations$lat, stations$lon, 
                                         lat_grid = seq(38.125, 70.125, .250),  lon_grid = seq(10.125, 35.125, .250))
  stations_fin = cbind(stations, stations_grid)
  stations_fin$biogeo = 'Boreal'  # All finish stations are in the Boreal biogeoregion


# Merge / rbind all finish data  ---------------------------
  
  # Merge for data.table automatically merges by the key collumns defined further up in the script
  fenodata_fin = rbind(
    merge(conmaj, stations),
    merge(tarax, stations)
  )
  

# Cleanup datasets  ---------------------------
  
  fenodata_fin = as.data.frame(fenodata_fin)[c('stid', 'species', 'event', 'year', 'doy')]
  stations_fin$stname = paste(stations_fin$city, stations_fin$subcity)  # for compatibility with the pheno-dataset
  stations_fin = as.data.frame(stations_fin)[
    c('stid', 'gridlat', 'gridlon', 'lat', 'stname', 'lon', 'alt', 'latdist', 'londist', 'biogeo')]
  

# Manually fixe data-errors ---------------------------
  
  # Remove two observations for the year 5003
  # This could be the year 2003, but for one of the two observations
  # data exists for 2003, so I decided both should be removed
  fenodata_fin[fenodata_fin$year == 5003,]
  fenodata_fin = fenodata_fin[fenodata_fin$year != 5003,]
  

save(fenodata_fin, stations_fin, file = './data/fenodata_fin.rda')
```


## Merge and save all phenological datasets

```{r}
data(fenodata)
data(fenodata_fin)
data(fenost_biogeo)

fenodata_all = rbind(fenodata, fenodata_fin) %>% 
  filter(event == 'BF',
         get_doy_month(.$doy, .$year) <= MAX_MONTH
         )
rm(fenodata, fenodata_fin)
fenodata_all = merge(fenodata_all, fenost_biogeo, by = 'stid', all.x = TRUE) 

save(fenodata_all, file = './data/fenodata_all.rda')
```



# Process EOBS climate data / Time dependent covariates

## Load and filter EOBS

(very slow, 15-20mins)

Filter criteria (implemented in extract_daily_eobs_from_ncdf):

* After 1970
* Before 2011
* Fenostation available for climate data (requires fenost dataset)

```{r, eval=FALSE}

```

## Process EOBS / time dependent covariates

(slow, a few minutes)

```{r, eval = TRUE}

```

## Calculate monthly aggregate statistics for time dependent covariates

(very slow, ~15min)

```{r, eval = TRUE}
data(td_covar)


# Prepare dataset and selecrt variables -------------
  td_covar$date = strtrim(td_covar$time, 6) 
  td_covar %<>%
    group_by(stid, date) 

  # Select variables to summarise
  vars     = match(c('tn', 'tx', 'tg', 'rr'), names(td_covar))
  

# Summarise (slow!) -----------
  
  tick = Sys.time()
  print('Summarising (slow)')
  td_covar_monthly = summarise_each(td_covar, calc_summary, vars)
  print(paste("Summarising completed after", format(difftime(Sys.time(), tick), digits=4)))

  
# Rename to fit with naming scheme in pheno dataset --------------
  setnames(td_covar_monthly, colnames(td_covar_monthly), gsub('_', '.', colnames(td_covar_monthly)))

  
# Add additonal variables for convenience ---------------------
  td_covar_monthly %<>% mutate(
    year = as.integer(strtrim(date, 4)),
    monthNo = as.integer(substr(date, 5,6))) %>% 
    as.data.frame()

  
save(td_covar_monthly, file = './data/td_covar_monthly.rda')

```


# Process NAO data

```{r}

data_dir = '/home/hoelk/Datasets/pheonosurvival/'
nao<- read.table(paste(data_dir, "nao/Naodata.csv", sep=""), header=TRUE, sep=",")

# Conver month to numeric and filter out months after August -------------------
  
  nao$monthNo[nao$month == 'jan'] = 1
  nao$monthNo[nao$month == 'feb'] = 2
  nao$monthNo[nao$month == 'mar'] = 3
  nao$monthNo[nao$month == 'apr'] = 4
  nao$monthNo[nao$month == 'may'] = 5
  nao$monthNo[nao$month == 'jun'] = 6
  nao$monthNo[nao$month == 'jul'] = 7
  nao$monthNo[nao$month == 'aug'] = 8
  nao$monthNo[nao$month == 'sep'] = 9
  nao$monthNo[nao$month == 'oct'] = 10
  nao$monthNo[nao$month == 'nov'] = 11
  nao$monthNo[nao$month == 'dec'] = 12
  
  nao[is.na(nao$monthNo),]
  
  nao$monthNo = as.integer(nao$monthNo)
  

# Calculate yearly nao aggregate statistics ------------

  nao_yearly = nao %>% 
    group_by(year) %>% 
    summarise_each(calc_summary, nao) %>% 
    as.data.frame()

  sub = 2:length(names(nao_yearly))
  names(nao_yearly)[sub] = paste('nao.', names(nao_yearly)[sub], sep = '')


save(nao, nao_yearly, file = './data/nao_covar.rda')
```


# Create dataset for other analyses (pheno)

Filter criteria:

  * Temperature data exists (precipitation can be missing)
  * Only months <= doyMonth are kept

```{r}
data(fenodata_all)
data(td_covar_monthly)
data(nao_covar)
data(pheno)

fenodata_all = data.table(fenodata_all, key=c('stid', 'year'))
td_covar_monthly = data.table(td_covar_monthly, key=c('stid', 'year'))

dat = merge(fenodata_all, td_covar_monthly, all.x = TRUE, all.y = FALSE, allow.cartesian = TRUE) %>%
  merge(nao_yearly, by = 'year') %>% 
  merge(nao, by = c('year', 'monthNo')) %>% 
  as.data.frame() %>% 
  mutate(code = substr(.$stid, 2,3),
         doyMonth = get_doy_month(.$doy, .$year)) %>% 
  extract(!is.na(.$date),)  %>% 
  extract(!is.na(.$tg.mean),) %>% 
  extract(.$monthNo <= .$doyMonth,)

pheno_all = dat

save(pheno_all, file="data/pheno_all.rda")

```


# Create test datasets

```{r}
data(fenost)
data(fenodata_all)
data(td_covar)

# Get the 10 stations with the most complete timeseries
  sub = fenodata_all %>%
    group_by(year) %>%
    count(stid) %>%
    extract(order(.$n, decreasing = TRUE),) %>%
    extract(1:10,)
  
  fenost_test = fenost[fenost$stid %in% sub$stid,] 
  
  fenodata_test = fenodata_all %>% 
    extract(.$year > 1970,) %>%
    extract(.$stid %in% sub$stid,)%>%
    extract(.$event == 'BF',) # only beginn of flowering events

# Filter EOBS climate data for locations
  sub = paste(fenost_test$gridlat, fenost_test$gridlon)
  td_covar_test = td_covar[paste(td_covar$lat, td_covar$lon) %in% sub,]

  save(fenodata_test, fenost_test, td_covar_test, file = "./data/feno_eobs_test.rda")
  rm(sub)
```


# Fetch data for maps

(slow, needs to fetch data from the web)

```{r, fig.height = 10, warning=FALSE, eval = FALSE}
data(fenost_biogeo)
data(td_covar)

## Get map data for E-OBS temperature stations ####
temp_stations = unique(as.data.frame(td_covar)[c('lat', 'lon')])
rm(td_covar)
feno_stations = unique(fenost[c('lat', 'lon')])
rm(fenost)

# Bounding box of data
bb = c(
  min(temp_stations$lon),
  min(temp_stations$lat),
  max(temp_stations$lon),
  max(temp_stations$lat))

str(bb)

 # Fetch basemap
 feno_map = get_map(location = bb, maptype="toner-lite")
 save(feno_stations, 
        temp_stations,
        feno_map, 
        bb, 
        file="data/feno_map.rda")
 
 
#  e  = attr(feno_map, "bb") %>% 
#     as.matrix %>% 
#     as.vector %>% 
#     extract(c(2,4,1,3)) %>% 
#     extent()
 
 e = extent(bb[c(1,3,2,4)])
 e[1] = 12.5
 e[2] = 32.5
 e[3] = e[3] - 5
 e[4] = e[4] + 5

# Crop biogeoregions to data
 
biogeo = readOGR('/home/hoelk/Datasets/pheonosurvival/biogeo/', 'BiogeoRegions2015')  %>% 
  spTransform(CRS("+init=epsg:4326"))  %>% 
  crop(e) %>% 
  ggplotify_spdf(region = 'short_name')

save(biogeo, file = 'data/biogeo.rda')
 
 
```



# Work in progress


### Comparison between pheno_all and pheno

```{r}

```



Is there climate data for all phenostations?

```{r}
data(fenodata_all)
load("~/Datasets/pheonosurvival/tg_complete.rda") 
load("~/Datasets/pheonosurvival/tn_complete.rda") 
load("~/Datasets/pheonosurvival/tx_complete.rda") 
load("~/Datasets/pheonosurvival/rr_complete.rda") 
load("~/Datasets/pheonosurvival/elev_complete.rda") 


stations_to_keep = fenodata_all[c('gridlat', 'gridlon', 'stid')] %>%  
  extract(!duplicated(.),)

stations_checklist = list(
  tg = table(paste(stations_to_keep$gridlat, stations_to_keep$gridlon) %in% paste(tg$lat, tg$lon)),
  tn = table(paste(stations_to_keep$gridlat, stations_to_keep$gridlon) %in% paste(tn$lat, tn$lon)),
  tx = table(paste(stations_to_keep$gridlat, stations_to_keep$gridlon) %in% paste(tx$lat, tx$lon)),
  rr = table(paste(stations_to_keep$gridlat, stations_to_keep$gridlon) %in% paste(rr$lat, rr$lon)),
  elev = table(paste(stations_to_keep$gridlat, stations_to_keep$gridlon) %in% paste(elev$lat, elev$lon)))

rm(tg, tn, tx, rr, elev)

nrow(stations_to_keep)
length(stations_checklist) 

# 3 stations are missing climate data?

```

## Covariates

```{r}

td_covar_monthly[is.na(td_covar_monthly$date),]


```



## Checks / Comments

Sometimes, two different fenostations at the same coordinates exist

```{r}
  # x = fenost_biogeo[duplicated(paste(fenost_all$lat, fenost_all$lon)),]
  # dups = fenost_all[paste(fenost_all$lat, fenost_all$lon) %in% paste(x$lat, x$lon),]
  # with(dups, dups[order(lat,lon),][c('stid', 'lat', 'lon', 'gridlat', 'gridlon', 'alt', 'biogeo')])
```





