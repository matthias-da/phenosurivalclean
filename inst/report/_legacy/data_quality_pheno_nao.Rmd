---
title: "Untitled"
author: "Stefan Fleck"
date: "September 10, 2015"
output: html_document
---

```{r, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_knit$set(root.dir = '../')
 # setwd('/home/hoelk/Dropbox/workspace/r/phenosurvival/')
```


```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(devtools)
load_all('./')

```

```{r}
data(pheno) # pheno data set i got from you
data(pheno_all) # pheno dataset i made (work in progress, see data_processing)
data(aggNaom) # nao data i got from you
data(aggNao)  # nao data i got from you

```

# Additional observations in pheno_all

```{r}
finnish_new = nrow(pheno_all[grep('pfi', pheno_all$stid),]) - nrow(pheno[grep('pfi', pheno$stid),])
sort(as.character(unique(pheno_all$stid[!pheno_all$stid %in% pheno$stid])))
```

* Observations pheno: `r nrow(pheno)`
* Observations pheno\_all: `r nrow(pheno_all)` 
* Difference: `r nrow(pheno_all) - nrow(pheno)`
* `r finnish_new` observations are due to the additional Finnish data
* There are differences in the number of months with data per station between pheno and pheno_all

```{r}
pheno %>% 
  group_by(stid) %>% 
  summarise(species = length(unique(species)),
            obs_months = length(unique(date))
            )

pheno_all %>% 
  group_by(stid) %>% 
  summarise(species = length(unique(species)),
            obs_months = length(unique(date))
            )
```

# Differences in Nao 

The yearly aggregated nao data in pheno does not match with aggNaom, aggNao, pheno_all.
Maybe there was a problem when merging the data?
The nao data in `pheno_all` comes directly from the csv (see `data_processing.Rmd`)

```{r}
pheno %>% 
  extract(.$year.x == '1971',) %>% 
  extract(1, c('year.x', 'nao.mean'))

aggNaom %>% 
  extract(20, c('year', 'nao.mean'))

unlist(aggNao['1971'])[1]
  
pheno_all %>% 
  extract(.$year == '1971',) %>% 
  extract(1, c('year', 'nao.mean'))

```

# Difference in monthly aggregation statistics

The pheno values are different from the values I calculated.

* Means only seems to be slightly off, 
* Kurtosis and Skewness sometimes differ a lot (see tables at the bottom of this document)


```{r}
data(pheno)
load("~/Datasets/pheonosurvival/tn_complete.rda") 

sub = pheno[c(10000),][c('gridlat', 'gridlon', 'date')]

tt = tn %>% 
  as.data.frame() %>% 
  extract(.$lon == sub$gridlon & .$lat == sub$gridlat,) %>% 
  mutate(date = strtrim(time, 6)) %>% 
  extract(.$date == sub$date,)

pt = pheno %>% 
  extract(.$gridlon == sub$gridlon & .$gridlat == sub$gridlat & .$date == sub$date,)

pt[c('tn.mean', 'tn.skewness', 'tn.kurtosis')][1,]
data.frame(
  tn.mean = mean(tt$value),
  tn.skewness = skewness(tt$value),
  tn.kurtosis = kurtosis(tt$value)
)

```

## More examples for differences between pheno and pheno_all 
```{r}
data(pheno)
load('./data/pheno_all.rda')

pheno_test = pheno %>% 
  as.data.frame() %>% 
  mutate(year = as.integer(year.x),
         nao = nao.x,
         monthNo = as.integer(as.character(monthNo))) 

pheno_all_test = as.data.frame(pheno_all)

rm(pheno, pheno_all)

# Ensure that both datasets have the same variables to make comparing easier
pheno_test = pheno_test[names(pheno_test)[names(pheno_test) %in% names(pheno_all_test)]]
pheno_all_test = pheno_all_test[names(pheno_all_test)[names(pheno_all_test) %in% names(pheno_test)]]

  # names(pheno_test)[!names(pheno_test) %in% names(pheno_all_test)]


# Enusre that both datasets have the same ovservations
  # table(pheno_all_test$stid %in% pheno_test$stid)

pheno_all_test %<>%
  extract(
    pheno_all_test$stid %in% pheno_test$stid &
    pheno_all_test$date %in% pheno_test$date,) %>% 
  extract(order(.$stid, .$date),)

pheno_test     = pheno_test[order(pheno_test$stid, pheno_test$date),]

sub = names(pheno_test)[1:30]
sub = c('date', 'stid', 'tn.mean', 'tg.mean', 'tx.mean', 'tx.skewness', 'tx.kurtosis')

pheno_all_test %>% 
  extract(sub) %>% 
  extract(!duplicated(.),) %>% 
  head(15) %>% 
  `row.names<-`(NULL)
  
pheno_test %>% 
  extract(sub) %>% 
  extract(!duplicated(.),) %>% 
  head(15) %>% 
  `row.names<-`(NULL)
```

