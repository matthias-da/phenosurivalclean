library(devtools)
library(stargazer)
data(fenodata_all)
load_all("./")

modelruns = list.files("./data/modelruns")

for (i in 1:length(modelruns)){

load(paste0('./data/modelruns/', modelruns[i]))

species = unique(fenodata_all$species)


pdf(file = paste0('/home/hoelk/Dropbox/workspace/r/phenosurivalclean/inst/plots/', modelruns[i], '.pdf'), 
    onefile = TRUE, 
    paper='A4r', 
    width=11, height=8.5)

  for(sp in species){
    print(sp)

    title = cox_data$coxph[[sp]]$call %>% 
      as.character %>% 
      extract(2) %>% 
      paste(sp,':', .)
    
    filename = paste0('cox_', sp)

    # Print cox model summary and generate text file
    stargazer(cox_data$coxph[[sp]], 
              apply.coef = exp,
              dep.var.labels   = "Hazard ratio",
              title = paste('Coxph results', sp),
              out = paste0('/home/hoelk/Dropbox/workspace/r/phenosurivalclean/inst/latextables/', filename, '.tex'), 
              single.row=TRUE)


    # Plot Kaplan-mayer curves
    
    
    plot(survfit(cox_data$coxph[[sp]]), mark.time = TRUE)
    title(title)

    # Plot Kaplan-mayer curves, grouped by decade
    counting = cox_data$counting[[sp]]
    counting$decade = (cox_data$counting[[sp]]$year %/% 10) * 10
    fitted = survfit(cox_data$surv[[sp]] ~ decade, data = counting)
    colfunc <- colorRampPalette(c("green", "red"))
    colors = colfunc(length(unique(counting$decade)))
    
    print(ggsurv(fitted, surv.col = colors, plot.cens = FALSE, main = title))
    
    rm(fitted)
    gc()
    
    # Survfit estimated median / restricted mean survival date
    res = survfit(cox_data$coxph[[sp]]) 

    number_of_strata = res %>% 
      summary %>% 
      extract2('table') %>% 
      rownames %>% 
      strsplit(', ') %>% 
      extract2(1) %>% 
      length()
      
    
    estimated_doy =  summary(res, rmean = 180)$table %>% 
        as.data.frame() %>% 
        extract(c('*rmean', 'median')) %>% 
        rename_('doy_rmean' = '`*rmean`',
                'doy_median' = 'median') %>% 
        mutate(strata = rownames(.)) 
  
    
    
    if (number_of_strata == 3){
      counting$strata = paste0('year=', counting$year, ', ', counting$biogeo, ', ', counting$altclass)
    } else if (number_of_strata == 2){
      counting$strata = paste0('year=', counting$year, ', ', counting$biogeo)
      }

   counting = merge(counting, estimated_doy, by = 'strata')
   
   p1 = counting %>% 
     ggplot(aes(
       x = doy,
       y = doy_rmean
     )) +
     geom_point(alpha = 0.01) +
     geom_smooth() +
     ggtitle(title)
   
   
   p2 = counting %>% 
     ggplot(aes(
       x = doy,
       y = doy_median
     )) +
     geom_point(alpha = 0.01) +
     geom_smooth() +
     ggtitle(title)
   
   print(p1)
   print(p2)
   rm(p1,p2)
  

  }

dev.off()

}
