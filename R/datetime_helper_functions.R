#' Get day of year
#' 
#' Get the day of year for a specific date supplied as 'char' object.
#'
#' @param x char or numeric object contatining the date of interest
#' @param format format of the char vector contating the date
#'
#' @return Day of year of the date of interest (integer)
#' @export
#'
#' @examples get_doy(19990112)
get_doy = function(x, format = '%Y%m%d'){
  day_of_interest = as.Date(as.character(x), format = format)
  date_to_doy(day_of_interest)
}

#' Get year
#' 
#' Get the year for a specific date supplied as a text string.
#'
#' @param char or numeric object contatining the date of interest
#' @param format format of the char vector contating the date
#'
#' @return Year of the date of interest (integer)
#' @export
#'
#' @examples get_year(19900112)
get_year = function(x, format = '%Y%m%d'){
  day_of_interest = as.Date(as.character(x), format = format)
  year = as.integer(strftime(day_of_interest, format = '%Y'))
}


#' get Datetime
#' 
#' Helper function for converting char objects to datetime objects. Specific
#' to the phenosurvival project
#'
#' @param char object contatining the date of interest
#' @param format format of the char vector contating the date
#'
#' @return Date as 'Date' object
#' @export
#'
#' @examples get_datetime(19900112)
get_datetime = function(x, format = '%Y%m%d'){
  day_of_interest = as.Date(as.character(x), format = format)
}
  
#' Day of year to date
#'
#' Calculate date based on day-of-year and year.
#'
#' @param doy Day of year
#' @param year Year
#'
#' @return Date as a 'Date' object
#' @export
#'
#' @examples doy_to_date(123, 2012)
doy_to_date = function(doy, year){
  origin = paste(year, '01-01', sep ='-')
  as.Date(doy - 1, origin = origin) 
}

get_doy_month = function(doy, year){
  res = doy_to_date(doy, year)
  res = as.integer(strftime(res, format = '%m'))
  return(res)
}


#' Date to doy
#' 
#' Get the day of year for a specific date supplied as 'Date' object 
#'
#' @param day_of_interest Day of interest as Date object
#'
#' @return Day of year of the date of interest (integer)
#' @export
#'
#' @examples date_to_doy(as.Date(('2012-01-02'))
date_to_doy = function(day_of_interest){
  day_zero_of_year = as.Date(paste(strftime(day_of_interest, format = '%Y'), '-01-01', sep =''))    
  return(as.integer(day_of_interest - day_zero_of_year + 1))
}
