#' merge nao data and data formatting
#' @param nao data from the CRU webpage: http://www.cru.uea.ac.uk/cru/data/nao/
#' @param period currently only year is supported
#' @author Matthias Templ
#' @export
#' @return merged and formatted data
merge_nao <- function(nao=nao, period="year"){
  res <- aggregate(nao[,"nao",drop=FALSE], list(nao$year), cs)
  m <- merge(nao, res, by.x="year", by.y="Group.1")
  date <- apply(m[,c("year","month")], 1, paste, collapse="")
  date <- gsub("jan", "01", date)
  date <- gsub("feb", "02", date)
  date <- gsub("mar", "03", date)
  date <- gsub("apr", "04", date)
  date <- gsub("may", "05", date)
  date <- gsub("jun", "06", date)
  date <- gsub("jul", "07", date)
  m$date <- date
  return(m)
}