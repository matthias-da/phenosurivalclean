coxph_run_to_file = function(fenodata_all, td_covar, savepath = 'data/modelruns/', comment = ''){

  # Generate counting datasets ---------------------
  cox_data = list()
  cox_data$counting = list()
  cox_data$surv     = list()
  cox_data$coxph    = list()

  species = unique(fenodata_all$species)

  for(sp in species){

    cox_data$counting[[sp]]   = prepare_cox_counting_data(fenodata_all, td_covar, species = sp)
    cox_data$surv[[sp]]       = with(cox_data$counting[[sp]], Surv(tstart, tstop, flowering, type = 'counting'))
    cox_data$coxph[[sp]]    = coxph(cox_data$surv[[sp]] ~ (gdd) + strata(year) + strata(biogeo),
                                    data = cox_data$counting[[sp]], control = coxph.control(iter.max = 100))

    rm(td_covar)
    rm(fenodata_all)
    gc()


  }


  # Save results -----------------------

  filename = 'data/coxph_data.rda'
  cat('Saving file to ')
  cat(filename)
  cat('.......')
  save(cox_data, file = filename )
  cat(' done.')

}

coxph_call_to_filename = function(call, savepath = '/data'){
  res = call %>%
    as.character() %>%
    extract(2) %>%
    strsplit('~ ') %>%
    extract2(1) %>%
    extract(2) %>%
    gsub(' ', '', ., fixed = TRUE) %>%
    gsub('+', '-', ., fixed = TRUE) %>%
    gsub('(', '__', ., fixed = TRUE) %>%
    gsub(')', '__', ., fixed = TRUE) %>%
    gsub('strata__', 'STR', ., fixed = TRUE) %>%
    paste0(savepath, ., '.rda') %>%
    gsub('__.rda', '.rda', ., fixed = TRUE)

  return(res)

}
